"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AalbmigrateApplication = void 0;
const tslib_1 = require("tslib");
const dist_1 = require("@loopback/boot/dist");
const dist_2 = require("@loopback/booter-lb3app/dist");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const rest_explorer_1 = require("@loopback/rest-explorer");
const path = tslib_1.__importStar(require("path"));
const sequence_1 = require("./sequence");
class AalbmigrateApplication extends (0, dist_1.BootMixin)(((0, repository_1.RepositoryMixin)(rest_1.RestApplication))) {
    constructor(options = {}) {
        super(options);
        debugger;
        // Set up the custom sequence
        this.sequence(sequence_1.MySequence);
        this.static('/', path.join(__dirname, '../public'));
        this.component(rest_explorer_1.RestExplorerComponent);
        // this.component(Lb3AppBooterComponent);
        this.component(dist_2.Lb3AppBooterComponent);
        const lb3AppPath = path.resolve(__dirname, '../lb3app');
        this.configure(dist_2.Lb3AppBooterComponent).to({
            projectRoot: lb3AppPath,
        });
        this.projectRoot = __dirname;
        // Customize @loopback/boot Booter Conventions here
        this.bootOptions = {
            controllers: {
                // Customize ControllerBooter Conventions here
                dirs: ['controllers'],
                extensions: ['.controller.js'],
                nested: true,
            },
            lb3app: {
                mode: 'fullApp',
                // path: path.resolve(__dirname, '.../lb3app/server/'),
                // path:'../../lb3app/server/',
            },
        };
    }
}
exports.AalbmigrateApplication = AalbmigrateApplication;
//# sourceMappingURL=application.js.map