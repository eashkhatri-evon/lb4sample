/// <reference types="node" />
import { ApplicationConfig } from '@loopback/core/dist';
import * as http from 'http';
import { AalbmigrateApplication } from './application';
export { ApplicationConfig };
export declare class ExpressServer {
    private app;
    readonly lbApp: AalbmigrateApplication;
    server?: http.Server;
    url: String;
    constructor(options?: ApplicationConfig);
    boot(): Promise<void>;
    start(): Promise<void>;
    stop(): Promise<void>;
}
