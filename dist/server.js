"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpressServer = void 0;
const tslib_1 = require("tslib");
const events_1 = require("events");
const express_1 = tslib_1.__importDefault(require("express"));
const path = tslib_1.__importStar(require("path"));
const application_1 = require("./application");
const loopback = require('loopback');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');
class ExpressServer {
    constructor(options = {}) {
        this.app = (0, express_1.default)();
        this.lbApp = new application_1.AalbmigrateApplication(options);
        // Middleware migrated from LoopBack 3
        this.app.use(loopback.favicon());
        this.app.use(compression());
        this.app.use(cors());
        this.app.use(helmet());
        // Mount the LB4 REST API
        this.app.use('/api', this.lbApp.requestHandler);
        // Custom Express routes
        // Serve static files in the public folder
        this.app.use(express_1.default.static(path.join(__dirname, '../public')));
    }
    async boot() {
        await this.lbApp.boot();
    }
    async start() {
        await this.lbApp.start();
        const port = this.lbApp.restServer.config.port || 3000;
        const host = this.lbApp.restServer.config.host || '127.0.0.1';
        this.server = this.app.listen(port, host);
        await (0, events_1.once)(this.server, 'listening');
        const add = this.server.address();
        this.url = `http://${add.address}:${add.port}`;
        console.log(" this.url", this.url);
    }
    async stop() {
        if (!this.server)
            return;
        await this.lbApp.stop();
        this.server.close();
        await (0, events_1.once)(this.server, 'close');
        this.server = undefined;
    }
}
exports.ExpressServer = ExpressServer;
//# sourceMappingURL=server.js.map