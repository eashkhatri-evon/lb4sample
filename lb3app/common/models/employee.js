'use strict';

module.exports = function(Employee) {
    Employee.getEmployeeData = function(value, cb) {
      console.log("value received:", value);
  
      // Simulate fetching data based on the input value
      const employeeData = {
        name: "John Doe",
        position: "Software Engineer",
        input: value
      };
  
      cb(null, employeeData);
    };
    Employee.postEmployeeData = function(data, cb) {
      console.log("data received:", data);
  
      // Simulate saving the employee data
      const savedData = {
        message: "Employee data saved successfully",
        savedData: data
      };
  
      cb(null, savedData);
    };
  
    Employee.remoteMethod(
      'getEmployeeData',
      {
        accepts: {arg: 'value', type: 'string', required: true},
        returns: {arg: 'data', type: 'object', root: true},
        http: {path: '/getEmployeeData', verb: 'get'}
      }
    );
    Employee.remoteMethod(
      'postEmployeeData',
      {
        accepts: {arg: 'data', type: 'object', required: true, http: {source: 'body'}},
        returns: {arg: 'result', type: 'object', root: true},
        http: {path: '/postEmployeeData', verb: 'post'}
      }
    );
  };