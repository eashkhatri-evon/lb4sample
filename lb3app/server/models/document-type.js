'use strict';


module.exports = function (DocumentType) {

    /**
     * autosuggest
     * @param {string} query 
     * @param {Function} callback 
     * @returns 
     */
    DocumentType.autosuggest = function (query, callback) {
        query = query ? query.trim() : '';
        return callback(null, "autosuggest response");
    }

    DocumentType.remoteMethod('autosuggest', {
        http: {
            verb: 'get',
            path: '/autosuggest'
        },
        accepts: [
            { arg: 'query', type: 'string' }
        ],
        returns: {
            arg: 'result',
            type: 'object'
        }
    });

    

};
