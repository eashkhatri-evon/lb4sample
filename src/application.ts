import {BootMixin} from '@loopback/boot/dist';
import {Lb3AppBooterComponent} from '@loopback/booter-lb3app/dist';
import {ApplicationConfig} from '@loopback/core/dist';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerComponent
} from '@loopback/rest-explorer';
import * as path from 'path';
import {MySequence} from './sequence';
export {ApplicationConfig};

export class AalbmigrateApplication extends BootMixin(
  (RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);
    debugger
    // Set up the custom sequence
    this.sequence(MySequence);
    this.static('/', path.join(__dirname, '../public'));

    this.component(RestExplorerComponent);
    // this.component(Lb3AppBooterComponent);
    this.component(Lb3AppBooterComponent);
    const lb3AppPath = path.resolve(__dirname, '../lb3app');

    this.configure(Lb3AppBooterComponent as any).to({
      projectRoot: lb3AppPath,
    });

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
      lb3app: {
        mode: 'fullApp',
        // path: path.resolve(__dirname, '.../lb3app/server/'),
        // path:'../../lb3app/server/',
      },
    };
  }
}
